<?php

require_once('animal.php');

class Ape extends animal
{
    public $get_legs = 2;

    public function yell(){
        return "Auooo";
    }

}