<?php

require_once 'animal.php';
require_once 'Ape.php';
require_once 'Frog.php';


echo "<br>";
echo "<strong>OUTPUT AKHIR </strong>";
echo "<br>";
echo "<br>";
echo "<br>";


$sheep = new Animal("shaun");

echo "Name : " . $sheep->get_name . "<br>"; // "shaun"
echo "Legs : " . $sheep->get_legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->get_cold_blooded . "<br>"; // "no"

echo "<br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())


$frog = new Frog("buduk");

echo "Name : " . $frog->get_name . "<br>"; // "shaun"
echo "Legs : " . $frog->get_legs . "<br>"; // 4
echo "Cold Blooded : " . $frog->get_cold_blooded . "<br>"; // "no"
echo "Jump : " . $frog->jump() . "<br>";

echo "<br>";

$ape = new Ape("kera sakti");

echo "Name : " . $ape->get_name . "<br>"; 
echo "Legs : " . $ape->get_legs . "<br>"; 
echo "Cold Blooded : " . $ape->get_cold_blooded . "<br>"; 
echo "Yell : " . $ape->yell() . "<br>";
?>